package ir.miare.androidcodechallenge.ui.fragment;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u001a\u001a\u00020\u001bH\u0002J\u001a\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\u001a\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\u001a\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\u000e\u0010 \u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\u0011R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0012\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f0\u0013\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00110\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019\u00a8\u0006\""}, d2 = {"Lir/miare/androidcodechallenge/ui/fragment/SharedRankingViewModel;", "Landroidx/lifecycle/ViewModel;", "playerLeagueUseCase", "Lir/miare/androidcodechallenge/domain/playerLeague/PlayerLeagueUseCase;", "sortUseCase", "Lir/miare/androidcodechallenge/domain/playerLeague/SortUseCase;", "(Lir/miare/androidcodechallenge/domain/playerLeague/PlayerLeagueUseCase;Lir/miare/androidcodechallenge/domain/playerLeague/SortUseCase;)V", "TAG", "", "kotlin.jvm.PlatformType", "_playerLeagueData", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lir/miare/androidcodechallenge/data/ResponseState;", "", "Lir/miare/androidcodechallenge/data/entity/League;", "_sortingState", "Lkotlinx/coroutines/flow/MutableStateFlow;", "Lir/miare/androidcodechallenge/data/constant/SortMode;", "playerLeagueData", "Lkotlinx/coroutines/flow/SharedFlow;", "getPlayerLeagueData", "()Lkotlinx/coroutines/flow/SharedFlow;", "sortingState", "Lkotlinx/coroutines/flow/StateFlow;", "getSortingState", "()Lkotlinx/coroutines/flow/StateFlow;", "getData", "", "sortByTotalGoals", "list", "sortLeaguesAndPlayers", "sortLeaguesByAveGoals", "updateSortingState", "sortingMode", "app_debug"})
public final class SharedRankingViewModel extends androidx.lifecycle.ViewModel {
    private final ir.miare.androidcodechallenge.domain.playerLeague.PlayerLeagueUseCase playerLeagueUseCase = null;
    private final ir.miare.androidcodechallenge.domain.playerLeague.SortUseCase sortUseCase = null;
    private final java.lang.String TAG = null;
    private final kotlinx.coroutines.flow.MutableStateFlow<ir.miare.androidcodechallenge.data.constant.SortMode> _sortingState = null;
    @org.jetbrains.annotations.NotNull
    private final kotlinx.coroutines.flow.StateFlow<ir.miare.androidcodechallenge.data.constant.SortMode> sortingState = null;
    private final kotlinx.coroutines.flow.MutableSharedFlow<ir.miare.androidcodechallenge.data.ResponseState<java.util.List<ir.miare.androidcodechallenge.data.entity.League>>> _playerLeagueData = null;
    @org.jetbrains.annotations.NotNull
    private final kotlinx.coroutines.flow.SharedFlow<ir.miare.androidcodechallenge.data.ResponseState<java.util.List<ir.miare.androidcodechallenge.data.entity.League>>> playerLeagueData = null;
    
    @javax.inject.Inject
    public SharedRankingViewModel(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.domain.playerLeague.PlayerLeagueUseCase playerLeagueUseCase, @org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.domain.playerLeague.SortUseCase sortUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.flow.StateFlow<ir.miare.androidcodechallenge.data.constant.SortMode> getSortingState() {
        return null;
    }
    
    public final void updateSortingState(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.constant.SortMode sortingMode) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.flow.SharedFlow<ir.miare.androidcodechallenge.data.ResponseState<java.util.List<ir.miare.androidcodechallenge.data.entity.League>>> getPlayerLeagueData() {
        return null;
    }
    
    private final void getData() {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortLeaguesByAveGoals(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortLeaguesAndPlayers(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortByTotalGoals(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list) {
        return null;
    }
}