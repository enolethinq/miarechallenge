package ir.miare.androidcodechallenge.di;

import java.lang.System;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\b\u0010\u0007\u001a\u00020\bH\u0007\u00a8\u0006\t"}, d2 = {"Lir/miare/androidcodechallenge/di/UseCaseModule;", "", "()V", "providePlayerLeagueUseCase", "Lir/miare/androidcodechallenge/domain/playerLeague/PlayerLeagueUseCase;", "playerLeagueRepository", "Lir/miare/androidcodechallenge/data/repository/PlayerLeagueRepository;", "provideSortUseCase", "Lir/miare/androidcodechallenge/domain/playerLeague/SortUseCase;", "app_debug"})
@dagger.Module
public final class UseCaseModule {
    @org.jetbrains.annotations.NotNull
    public static final ir.miare.androidcodechallenge.di.UseCaseModule INSTANCE = null;
    
    private UseCaseModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final ir.miare.androidcodechallenge.domain.playerLeague.PlayerLeagueUseCase providePlayerLeagueUseCase(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.repository.PlayerLeagueRepository playerLeagueRepository) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final ir.miare.androidcodechallenge.domain.playerLeague.SortUseCase provideSortUseCase() {
        return null;
    }
}