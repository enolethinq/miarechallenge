package ir.miare.androidcodechallenge.di;

import java.lang.System;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007J\u0012\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bH\u0007J\u0018\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0004H\u0007\u00a8\u0006\r"}, d2 = {"Lir/miare/androidcodechallenge/di/NetworkModule;", "", "()V", "provideGsonBuilder", "Lcom/google/gson/Gson;", "providePlayerLeagueWebService", "Lir/miare/androidcodechallenge/data/remote/webService/PlayerLeagueWebService;", "retrofit", "Lretrofit2/Retrofit$Builder;", "provideRetrofit", "okHttpClient", "Lokhttp3/OkHttpClient;", "gson", "app_debug"})
@dagger.Module
public final class NetworkModule {
    @org.jetbrains.annotations.NotNull
    public static final ir.miare.androidcodechallenge.di.NetworkModule INSTANCE = null;
    
    private NetworkModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final com.google.gson.Gson provideGsonBuilder() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Named(value = "retrofit-regular")
    public final retrofit2.Retrofit.Builder provideRetrofit(@org.jetbrains.annotations.NotNull
    okhttp3.OkHttpClient okHttpClient, @org.jetbrains.annotations.NotNull
    com.google.gson.Gson gson) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final ir.miare.androidcodechallenge.data.remote.webService.PlayerLeagueWebService providePlayerLeagueWebService(@org.jetbrains.annotations.NotNull
    @javax.inject.Named(value = "retrofit-regular")
    retrofit2.Retrofit.Builder retrofit) {
        return null;
    }
}