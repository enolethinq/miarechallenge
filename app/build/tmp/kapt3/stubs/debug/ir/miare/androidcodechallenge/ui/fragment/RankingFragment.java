package ir.miare.androidcodechallenge.ui.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0002J&\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\b\u0010 \u001a\u00020\u0017H\u0016J\u001a\u0010!\u001a\u00020\u00172\u0006\u0010\"\u001a\u00020\u00192\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\"\u0010#\u001a\u00020\u00172\u000e\u0010$\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f2\b\b\u0002\u0010%\u001a\u00020&H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\u00078BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\'"}, d2 = {"Lir/miare/androidcodechallenge/ui/fragment/RankingFragment;", "Landroidx/fragment/app/Fragment;", "()V", "TAG", "", "kotlin.jvm.PlatformType", "_binding", "Lir/miare/androidcodechallenge/databinding/FragmentRankingBinding;", "binding", "getBinding", "()Lir/miare/androidcodechallenge/databinding/FragmentRankingBinding;", "dataList", "", "Lir/miare/androidcodechallenge/data/entity/League;", "leagueAdapter", "Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter;", "viewModel", "Lir/miare/androidcodechallenge/ui/fragment/SharedRankingViewModel;", "getViewModel", "()Lir/miare/androidcodechallenge/ui/fragment/SharedRankingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "initObservers", "", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onViewCreated", "view", "setupRv", "list", "hideHeader", "", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint
public final class RankingFragment extends androidx.fragment.app.Fragment {
    private final java.lang.String TAG = null;
    private final kotlin.Lazy viewModel$delegate = null;
    private ir.miare.androidcodechallenge.databinding.FragmentRankingBinding _binding;
    private java.util.List<ir.miare.androidcodechallenge.data.entity.League> dataList;
    private ir.miare.androidcodechallenge.ui.adapter.LeaguePlayerAdapter leagueAdapter;
    
    public RankingFragment() {
        super();
    }
    
    private final ir.miare.androidcodechallenge.ui.fragment.SharedRankingViewModel getViewModel() {
        return null;
    }
    
    private final ir.miare.androidcodechallenge.databinding.FragmentRankingBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initObservers() {
    }
    
    private final void setupRv(java.util.List<ir.miare.androidcodechallenge.data.entity.League> list, boolean hideHeader) {
    }
    
    @java.lang.Override
    public void onDestroyView() {
    }
}