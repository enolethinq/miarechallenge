package ir.miare.androidcodechallenge.data.extension;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 2, d1 = {"\u00008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0005\u001a\n\u0010\u0006\u001a\u00020\u0007*\u00020\b\u001a\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n*\b\u0012\u0004\u0012\u00020\f0\n\u001a\n\u0010\r\u001a\u00020\u000e*\u00020\u000f\u00a8\u0006\u0010"}, d2 = {"toLeague", "Lir/miare/androidcodechallenge/data/entity/League;", "Lir/miare/androidcodechallenge/data/entity/LeagueData;", "toLeagueEntity", "Lir/miare/androidcodechallenge/data/entity/LeagueEntity;", "Lir/miare/androidcodechallenge/data/remote/LeagueResponse;", "toPlayerEntity", "Lir/miare/androidcodechallenge/data/entity/PlayerEntity;", "Lir/miare/androidcodechallenge/data/remote/PlayerResponse;", "toPlayerLeagueEntityList", "", "Lir/miare/androidcodechallenge/data/entity/ListItem;", "Lir/miare/androidcodechallenge/data/remote/PlayerLeagueResponse;", "toTeamEntity", "Lir/miare/androidcodechallenge/data/entity/TeamEntity;", "Lir/miare/androidcodechallenge/data/remote/TeamResponse;", "app_debug"})
public final class ModelExtensionKt {
    
    @org.jetbrains.annotations.NotNull
    public static final java.util.List<ir.miare.androidcodechallenge.data.entity.ListItem> toPlayerLeagueEntityList(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.remote.PlayerLeagueResponse> $this$toPlayerLeagueEntityList) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public static final ir.miare.androidcodechallenge.data.entity.PlayerEntity toPlayerEntity(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.remote.PlayerResponse $this$toPlayerEntity) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public static final ir.miare.androidcodechallenge.data.entity.TeamEntity toTeamEntity(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.remote.TeamResponse $this$toTeamEntity) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public static final ir.miare.androidcodechallenge.data.entity.LeagueEntity toLeagueEntity(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.remote.LeagueResponse $this$toLeagueEntity) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public static final ir.miare.androidcodechallenge.data.entity.League toLeague(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.entity.LeagueData $this$toLeague) {
        return null;
    }
}