package ir.miare.androidcodechallenge.ui.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00172\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0017\u0018\u0019\u001aB\u000f\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\r\u001a\u00020\fH\u0016J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\fH\u0016J\u0018\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\fH\u0016R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0002X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter;", "Landroidx/recyclerview/widget/ListAdapter;", "Lir/miare/androidcodechallenge/data/entity/League;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "hideHeader", "", "(Z)V", "TAG", "", "kotlin.jvm.PlatformType", "item", "lastHeaderPosition", "", "getItemCount", "getItemViewType", "position", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Companion", "LeagueDiffCallback", "LeagueViewHolder", "PlayerViewHolder", "app_debug"})
public final class LeaguePlayerAdapter extends androidx.recyclerview.widget.ListAdapter<ir.miare.androidcodechallenge.data.entity.League, androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    private final boolean hideHeader = false;
    private final java.lang.String TAG = null;
    private int lastHeaderPosition = 1;
    @org.jetbrains.annotations.NotNull
    public static final ir.miare.androidcodechallenge.ui.adapter.LeaguePlayerAdapter.Companion Companion = null;
    private static final int VIEW_TYPE_LEAGUE = 0;
    private static final int VIEW_TYPE_PLAYER = 1;
    private static final int VIEW_TYPE_ONLY_PLAYER = 2;
    private ir.miare.androidcodechallenge.data.entity.League item;
    
    public LeaguePlayerAdapter() {
        super(null);
    }
    
    public LeaguePlayerAdapter(boolean hideHeader) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override
    public int getItemViewType(int position) {
        return 0;
    }
    
    @java.lang.Override
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter$PlayerViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lir/miare/androidcodechallenge/databinding/ItemPlayerBinding;", "(Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter;Lir/miare/androidcodechallenge/databinding/ItemPlayerBinding;)V", "bindPlayer", "", "entity", "Lir/miare/androidcodechallenge/data/entity/Player;", "app_debug"})
    public final class PlayerViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final ir.miare.androidcodechallenge.databinding.ItemPlayerBinding binding = null;
        
        public PlayerViewHolder(@org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.databinding.ItemPlayerBinding binding) {
            super(null);
        }
        
        public final void bindPlayer(@org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.data.entity.Player entity) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter$LeagueViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lir/miare/androidcodechallenge/databinding/ItemLeagueBinding;", "(Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter;Lir/miare/androidcodechallenge/databinding/ItemLeagueBinding;)V", "bindLeague", "", "entity", "Lir/miare/androidcodechallenge/data/entity/League;", "app_debug"})
    public final class LeagueViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final ir.miare.androidcodechallenge.databinding.ItemLeagueBinding binding = null;
        
        public LeagueViewHolder(@org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.databinding.ItemLeagueBinding binding) {
            super(null);
        }
        
        public final void bindLeague(@org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.data.entity.League entity) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016J\u0018\u0010\b\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\t"}, d2 = {"Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter$LeagueDiffCallback;", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lir/miare/androidcodechallenge/data/entity/League;", "()V", "areContentsTheSame", "", "oldItem", "newItem", "areItemsTheSame", "app_debug"})
    public static final class LeagueDiffCallback extends androidx.recyclerview.widget.DiffUtil.ItemCallback<ir.miare.androidcodechallenge.data.entity.League> {
        
        public LeagueDiffCallback() {
            super();
        }
        
        @java.lang.Override
        public boolean areItemsTheSame(@org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.data.entity.League oldItem, @org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.data.entity.League newItem) {
            return false;
        }
        
        @java.lang.Override
        public boolean areContentsTheSame(@org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.data.entity.League oldItem, @org.jetbrains.annotations.NotNull
        ir.miare.androidcodechallenge.data.entity.League newItem) {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lir/miare/androidcodechallenge/ui/adapter/LeaguePlayerAdapter$Companion;", "", "()V", "VIEW_TYPE_LEAGUE", "", "VIEW_TYPE_ONLY_PLAYER", "VIEW_TYPE_PLAYER", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}