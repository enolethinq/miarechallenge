package ir.miare.androidcodechallenge.data.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0001\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\u000e\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006\u001a"}, d2 = {"Lir/miare/androidcodechallenge/data/remote/PlayerLeagueResponse;", "", "league", "Lir/miare/androidcodechallenge/data/remote/LeagueResponse;", "players", "", "Lir/miare/androidcodechallenge/data/remote/PlayerResponse;", "(Lir/miare/androidcodechallenge/data/remote/LeagueResponse;Ljava/util/List;)V", "getLeague", "()Lir/miare/androidcodechallenge/data/remote/LeagueResponse;", "setLeague", "(Lir/miare/androidcodechallenge/data/remote/LeagueResponse;)V", "getPlayers", "()Ljava/util/List;", "setPlayers", "(Ljava/util/List;)V", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_debug"})
public final class PlayerLeagueResponse {
    @org.jetbrains.annotations.NotNull
    private ir.miare.androidcodechallenge.data.remote.LeagueResponse league;
    @org.jetbrains.annotations.NotNull
    private java.util.List<ir.miare.androidcodechallenge.data.remote.PlayerResponse> players;
    
    @org.jetbrains.annotations.NotNull
    public final ir.miare.androidcodechallenge.data.remote.PlayerLeagueResponse copy(@org.jetbrains.annotations.NotNull
    @com.fasterxml.jackson.annotation.JsonProperty(value = "league")
    ir.miare.androidcodechallenge.data.remote.LeagueResponse league, @org.jetbrains.annotations.NotNull
    @com.fasterxml.jackson.annotation.JsonProperty(value = "players")
    java.util.List<ir.miare.androidcodechallenge.data.remote.PlayerResponse> players) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public PlayerLeagueResponse(@org.jetbrains.annotations.NotNull
    @com.fasterxml.jackson.annotation.JsonProperty(value = "league")
    ir.miare.androidcodechallenge.data.remote.LeagueResponse league, @org.jetbrains.annotations.NotNull
    @com.fasterxml.jackson.annotation.JsonProperty(value = "players")
    java.util.List<ir.miare.androidcodechallenge.data.remote.PlayerResponse> players) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final ir.miare.androidcodechallenge.data.remote.LeagueResponse component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final ir.miare.androidcodechallenge.data.remote.LeagueResponse getLeague() {
        return null;
    }
    
    public final void setLeague(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.remote.LeagueResponse p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<ir.miare.androidcodechallenge.data.remote.PlayerResponse> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<ir.miare.androidcodechallenge.data.remote.PlayerResponse> getPlayers() {
        return null;
    }
    
    public final void setPlayers(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.remote.PlayerResponse> p0) {
    }
}