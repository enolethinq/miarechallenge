package ir.miare.androidcodechallenge.domain.playerLeague;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016J\u001c\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016J\u001c\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016\u00a8\u0006\t"}, d2 = {"Lir/miare/androidcodechallenge/domain/playerLeague/SortUseCaseImpl;", "Lir/miare/androidcodechallenge/domain/playerLeague/SortUseCase;", "()V", "sortByTotalGoals", "", "Lir/miare/androidcodechallenge/data/entity/League;", "list", "sortLeaguesAndPlayers", "sortLeaguesByAveGoals", "app_debug"})
public final class SortUseCaseImpl implements ir.miare.androidcodechallenge.domain.playerLeague.SortUseCase {
    
    public SortUseCaseImpl() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortLeaguesByAveGoals(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortLeaguesAndPlayers(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortByTotalGoals(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list) {
        return null;
    }
}