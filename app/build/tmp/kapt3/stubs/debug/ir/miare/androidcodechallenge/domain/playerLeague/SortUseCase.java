package ir.miare.androidcodechallenge.domain.playerLeague;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H&J\u001c\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H&J\u001c\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H&\u00a8\u0006\b"}, d2 = {"Lir/miare/androidcodechallenge/domain/playerLeague/SortUseCase;", "", "sortByTotalGoals", "", "Lir/miare/androidcodechallenge/data/entity/League;", "list", "sortLeaguesAndPlayers", "sortLeaguesByAveGoals", "app_debug"})
public abstract interface SortUseCase {
    
    @org.jetbrains.annotations.NotNull
    public abstract java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortLeaguesByAveGoals(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list);
    
    @org.jetbrains.annotations.NotNull
    public abstract java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortLeaguesAndPlayers(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list);
    
    @org.jetbrains.annotations.NotNull
    public abstract java.util.List<ir.miare.androidcodechallenge.data.entity.League> sortByTotalGoals(@org.jetbrains.annotations.NotNull
    java.util.List<ir.miare.androidcodechallenge.data.entity.League> list);
}