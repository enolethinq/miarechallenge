package ir.miare.androidcodechallenge.ui.bottomsheet;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J&\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lir/miare/androidcodechallenge/ui/bottomsheet/PlayerInfoBottomSheet;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "player", "Lir/miare/androidcodechallenge/data/entity/PlayerEntity;", "(Lir/miare/androidcodechallenge/data/entity/PlayerEntity;)V", "binding", "Lir/miare/androidcodechallenge/databinding/BottomSheetPlayerInfoBinding;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class PlayerInfoBottomSheet extends com.google.android.material.bottomsheet.BottomSheetDialogFragment {
    private final ir.miare.androidcodechallenge.data.entity.PlayerEntity player = null;
    private ir.miare.androidcodechallenge.databinding.BottomSheetPlayerInfoBinding binding;
    
    public PlayerInfoBottomSheet(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.entity.PlayerEntity player) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
}