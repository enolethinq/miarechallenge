package ir.miare.androidcodechallenge.domain.playerLeague;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J#\u0010\u0005\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00070\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000b"}, d2 = {"Lir/miare/androidcodechallenge/domain/playerLeague/PlayerLeagueUseCaseImpl;", "Lir/miare/androidcodechallenge/domain/playerLeague/PlayerLeagueUseCase;", "repository", "Lir/miare/androidcodechallenge/data/repository/PlayerLeagueRepository;", "(Lir/miare/androidcodechallenge/data/repository/PlayerLeagueRepository;)V", "getData", "Lkotlinx/coroutines/flow/Flow;", "Lir/miare/androidcodechallenge/data/ResponseState;", "", "Lir/miare/androidcodechallenge/data/entity/League;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class PlayerLeagueUseCaseImpl implements ir.miare.androidcodechallenge.domain.playerLeague.PlayerLeagueUseCase {
    private final ir.miare.androidcodechallenge.data.repository.PlayerLeagueRepository repository = null;
    
    @javax.inject.Inject
    public PlayerLeagueUseCaseImpl(@org.jetbrains.annotations.NotNull
    ir.miare.androidcodechallenge.data.repository.PlayerLeagueRepository repository) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public java.lang.Object getData(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<? extends ir.miare.androidcodechallenge.data.ResponseState<? extends java.util.List<ir.miare.androidcodechallenge.data.entity.League>>>> continuation) {
        return null;
    }
}