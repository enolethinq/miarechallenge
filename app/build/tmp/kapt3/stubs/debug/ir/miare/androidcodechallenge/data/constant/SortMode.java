package ir.miare.androidcodechallenge.data.constant;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006\u00a8\u0006\u0007"}, d2 = {"Lir/miare/androidcodechallenge/data/constant/SortMode;", "", "(Ljava/lang/String;I)V", "NONE", "TEAM_LEAGUE_RANKING", "MOST_SCORE", "AVERAGE_PER_MATCH", "app_debug"})
public enum SortMode {
    /*public static final*/ NONE /* = new NONE() */,
    /*public static final*/ TEAM_LEAGUE_RANKING /* = new TEAM_LEAGUE_RANKING() */,
    /*public static final*/ MOST_SCORE /* = new MOST_SCORE() */,
    /*public static final*/ AVERAGE_PER_MATCH /* = new AVERAGE_PER_MATCH() */;
    
    SortMode() {
    }
}