package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

@ProcessedRootSentinel(
    roots = "ir.miare.androidcodechallenge.MyApp"
)
public final class _ir_miare_androidcodechallenge_MyApp {
}
