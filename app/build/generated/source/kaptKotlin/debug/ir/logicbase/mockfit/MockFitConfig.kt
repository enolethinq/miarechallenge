package ir.logicbase.mockfit

import kotlin.String
import kotlin.collections.Map
import kotlin.jvm.JvmField

public object MockFitConfig {
  @JvmField
  public val REQUEST_TO_JSON: Map<String, String> = mapOf(
      "[GET] list" to "data.json"
      )
}
