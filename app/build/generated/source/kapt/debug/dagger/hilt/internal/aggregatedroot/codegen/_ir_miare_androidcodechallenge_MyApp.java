package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "ir.miare.androidcodechallenge.MyApp",
    rootPackage = "ir.miare.androidcodechallenge",
    originatingRoot = "ir.miare.androidcodechallenge.MyApp",
    originatingRootPackage = "ir.miare.androidcodechallenge",
    rootAnnotation = HiltAndroidApp.class,
    rootSimpleNames = "MyApp",
    originatingRootSimpleNames = "MyApp"
)
public class _ir_miare_androidcodechallenge_MyApp {
}
