package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "ir.miare.androidcodechallenge.ui.fragment.RankingFragment_GeneratedInjector"
)
public class _ir_miare_androidcodechallenge_ui_fragment_RankingFragment_GeneratedInjector {
}
