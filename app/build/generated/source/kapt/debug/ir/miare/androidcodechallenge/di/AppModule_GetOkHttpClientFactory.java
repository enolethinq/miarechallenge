// Generated by Dagger (https://dagger.dev).
package ir.miare.androidcodechallenge.di;

import android.content.res.Resources;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.inject.Provider;
import okhttp3.OkHttpClient;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_GetOkHttpClientFactory implements Factory<OkHttpClient> {
  private final Provider<Resources> resourcesProvider;

  public AppModule_GetOkHttpClientFactory(Provider<Resources> resourcesProvider) {
    this.resourcesProvider = resourcesProvider;
  }

  @Override
  public OkHttpClient get() {
    return getOkHttpClient(resourcesProvider.get());
  }

  public static AppModule_GetOkHttpClientFactory create(Provider<Resources> resourcesProvider) {
    return new AppModule_GetOkHttpClientFactory(resourcesProvider);
  }

  public static OkHttpClient getOkHttpClient(Resources resources) {
    return Preconditions.checkNotNullFromProvides(AppModule.INSTANCE.getOkHttpClient(resources));
  }
}
