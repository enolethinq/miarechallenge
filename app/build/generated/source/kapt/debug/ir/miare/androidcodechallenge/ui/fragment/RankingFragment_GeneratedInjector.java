package ir.miare.androidcodechallenge.ui.fragment;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = RankingFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface RankingFragment_GeneratedInjector {
  void injectRankingFragment(RankingFragment rankingFragment);
}
