package ir.miare.androidcodechallenge

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.whenCreated
import dagger.hilt.android.AndroidEntryPoint
import ir.miare.androidcodechallenge.data.constant.SortMode
import ir.miare.androidcodechallenge.databinding.ActivityMainBinding
import ir.miare.androidcodechallenge.ui.fragment.RankingFragment
import ir.miare.androidcodechallenge.ui.fragment.SharedRankingViewModel
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val TAG = MainActivity::class.java.simpleName

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding
    
    private val viewModel: SharedRankingViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        binding?.let { setContentView(it.root) }
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, RankingFragment())
            .commit()

        binding?.radioGroup?.setOnCheckedChangeListener { group, checkedId->
            Log.d(TAG, "onCreate: checked ${group.checkedRadioButtonId}")
            when (binding?.radioGroup?.checkedRadioButtonId) {
                binding?.rbNone?.id -> {
                    viewModel.updateSortingState(SortMode.NONE)
                }
                binding?.rbAve?.id -> {
                    viewModel.updateSortingState(SortMode.AVERAGE_PER_MATCH)
                }
                binding?.rbRank?.id -> {
                    viewModel.updateSortingState(SortMode.TEAM_LEAGUE_RANKING)
                }
                binding?.rbScore?.id -> {
                    viewModel.updateSortingState(SortMode.MOST_SCORE)
                }
            }
        }

        initObservers()

    }

    private fun initObservers() {
        Log.i(TAG, "initObservers: ")
        lifecycleScope.launchWhenCreated {
            repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.sortingState.collectLatest {
                    when (it) {
                        SortMode.NONE -> {
                            binding?.rbNone?.isChecked = true
                        }
                        SortMode.TEAM_LEAGUE_RANKING -> {
                            binding?.rbRank?.isChecked = true
                        }
                        SortMode.MOST_SCORE -> {
                            binding?.rbScore?.isChecked = true
                        }
                        SortMode.AVERAGE_PER_MATCH -> {
                            binding?.rbAve?.isChecked = true
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding.let { _binding = null }
    }
}
