package ir.miare.androidcodechallenge.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.miare.androidcodechallenge.data.remote.webService.PlayerLeagueWebService
import ir.miare.androidcodechallenge.data.repository.PlayerLeagueRepository
import ir.miare.androidcodechallenge.data.repository.PlayerLeagueRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun providePlayerLeagueRepository(webService: PlayerLeagueWebService): PlayerLeagueRepository {
        return PlayerLeagueRepositoryImpl(webService)
    }
}