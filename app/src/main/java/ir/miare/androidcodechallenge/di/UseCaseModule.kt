package ir.miare.androidcodechallenge.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.miare.androidcodechallenge.data.repository.PlayerLeagueRepository
import ir.miare.androidcodechallenge.domain.playerLeague.PlayerLeagueUseCase
import ir.miare.androidcodechallenge.domain.playerLeague.PlayerLeagueUseCaseImpl
import ir.miare.androidcodechallenge.domain.playerLeague.SortUseCase
import ir.miare.androidcodechallenge.domain.playerLeague.SortUseCaseImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {
    @Singleton
    @Provides
    fun providePlayerLeagueUseCase(playerLeagueRepository: PlayerLeagueRepository): PlayerLeagueUseCase {
        return PlayerLeagueUseCaseImpl(playerLeagueRepository) as PlayerLeagueUseCase
    }

    @Singleton
    @Provides
    fun provideSortUseCase(): SortUseCase {
        return SortUseCaseImpl() as SortUseCase
    }
}