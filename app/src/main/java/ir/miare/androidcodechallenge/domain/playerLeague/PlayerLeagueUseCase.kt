package ir.miare.androidcodechallenge.domain.playerLeague

import ir.miare.androidcodechallenge.data.ResponseState
import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.data.entity.LeagueData
import ir.miare.androidcodechallenge.data.entity.ListItem
import ir.miare.androidcodechallenge.data.repository.PlayerLeagueRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface PlayerLeagueUseCase {
    suspend fun getData(): Flow<ResponseState<List<League>>>
}

class PlayerLeagueUseCaseImpl @Inject constructor(private val repository: PlayerLeagueRepository): PlayerLeagueUseCase {
    override suspend fun getData() = repository.getData()
}