package ir.miare.androidcodechallenge.domain.playerLeague

import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.data.entity.Player

interface SortUseCase {
    fun sortLeaguesByAveGoals(list: List<League>): List<League>
    fun sortLeaguesAndPlayers(list: List<League>): List<League>
    fun sortByTotalGoals(list: List<League>): List<League>
}

class SortUseCaseImpl: SortUseCase {
    override fun sortLeaguesByAveGoals(list: List<League>): List<League> {
        return list.sortedByDescending { league ->
            val totalGoals = league.players.sumOf { player -> player.total_goal }
            val averageGoals = totalGoals.toDouble() / league.total_matches
            averageGoals
        }
    }

    override fun sortLeaguesAndPlayers(list: List<League>): List<League> {
        val playerComparator = compareBy<Player> { it.team.rank }
        val leagueComparator = compareBy<League> { it.rank }

        return list.sortedWith(leagueComparator).map { league ->
            league.copy(players = league.players.sortedWith(playerComparator))
        }
    }

    override fun sortByTotalGoals(list: List<League>): List<League> {
        val allPlayers = arrayListOf<Player>()
        list.forEach {
            allPlayers.addAll(it.players)
        }
        return listOf(League(players = allPlayers.sortedByDescending { it.total_goal }))
    }

}