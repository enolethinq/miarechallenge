package ir.miare.androidcodechallenge.data.remote.webService

import ir.logicbase.mockfit.Mock
import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.data.entity.LeagueData
import ir.miare.androidcodechallenge.data.entity.ListItem
import ir.miare.androidcodechallenge.data.remote.PlayerLeagueResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.GET

interface PlayerLeagueWebService {

    @Mock("data.json")
    @GET("list")
    suspend fun getData(): Response<List<LeagueData>>
}