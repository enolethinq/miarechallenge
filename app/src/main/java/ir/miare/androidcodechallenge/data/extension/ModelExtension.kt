package ir.miare.androidcodechallenge.data.extension

import android.util.Log
import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.data.entity.LeagueData
import ir.miare.androidcodechallenge.data.entity.LeagueEntity
import ir.miare.androidcodechallenge.data.entity.ListItem
import ir.miare.androidcodechallenge.data.entity.PlayerEntity
import ir.miare.androidcodechallenge.data.entity.TeamEntity
import ir.miare.androidcodechallenge.data.remote.LeagueResponse
import ir.miare.androidcodechallenge.data.remote.PlayerLeagueResponse
import ir.miare.androidcodechallenge.data.remote.PlayerResponse
import ir.miare.androidcodechallenge.data.remote.TeamResponse

fun List<PlayerLeagueResponse>.toPlayerLeagueEntityList(): List<ListItem> {
    val list = arrayListOf<ListItem>()
    this.forEach {
        list.add(it.league.toLeagueEntity())
        list.addAll(it.players.map { it.toPlayerEntity() })
    }
    return list
}

fun PlayerResponse.toPlayerEntity(): PlayerEntity {
    return PlayerEntity(
        name = name,
        team = team.toTeamEntity(),
        totalGoal = total_goal
    )
}

fun TeamResponse.toTeamEntity(): TeamEntity {
    return TeamEntity(name, rank)
}

fun LeagueResponse.toLeagueEntity(): LeagueEntity {
    return LeagueEntity(
        name = name,
        country = country,
        rank = rank,
        total_matches = total_matches
    )
}

fun LeagueData.toLeague(): League {
    return League(
        name = league.name,
        country = league.country,
        rank = league.rank,
        total_matches = league.total_matches,
        players
    )
}


/*fun PlayerLeagueResponse.toPlayerLeagueEntity(): PlayerLeagueEntity {
    return PlayerLeagueEntity(
        leagueLabel = this.league.name.plus(" - ").plus(this.league.country),
        leagueRanking = this.league.rank,
        player = this.players.map { it.toPlayerEntity() },
        totalMatches = this.league.total_matches,
        averageGoalPerLeague = (this.players.sumOf { it.total_goal } / (this.league.total_matches?:1)).toDouble()
    )
}*/