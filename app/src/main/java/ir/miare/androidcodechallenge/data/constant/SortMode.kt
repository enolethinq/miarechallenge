package ir.miare.androidcodechallenge.data.constant

enum class SortMode {
    NONE, TEAM_LEAGUE_RANKING, MOST_SCORE, AVERAGE_PER_MATCH
}