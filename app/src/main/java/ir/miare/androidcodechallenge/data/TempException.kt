package ir.miare.androidcodechallenge.data

import kotlin.Exception

data class TempException(
    val errorMessage: String = ""
) : Exception()