package ir.miare.androidcodechallenge.data

sealed class ResponseState<out T>(val data: T? = null, val exception: TempException? = null) {
    class Success<T>(data: T? = null) : ResponseState<T>(data)
    class Error<T>(exception: TempException, data: T? = null) : ResponseState<T>(data, exception)
    object Loading : ResponseState<Nothing>()
}