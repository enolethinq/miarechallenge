package ir.miare.androidcodechallenge.data.repository

import android.util.Log
import ir.miare.androidcodechallenge.data.ResponseState
import ir.miare.androidcodechallenge.data.TempException
import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.data.entity.LeagueData
import ir.miare.androidcodechallenge.data.entity.ListItem
import ir.miare.androidcodechallenge.data.extension.toLeague
import ir.miare.androidcodechallenge.data.extension.toPlayerLeagueEntityList
import ir.miare.androidcodechallenge.data.remote.webService.PlayerLeagueWebService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

interface PlayerLeagueRepository {
    suspend fun getData(): Flow<ResponseState<List<League>>>
}

class PlayerLeagueRepositoryImpl @Inject constructor(private val webService: PlayerLeagueWebService): PlayerLeagueRepository {
    override suspend fun getData() = flow {
        try {
            emit(ResponseState.Loading)
            val response = webService.getData()
            if (response.isSuccessful) {
                Log.d("TAG", "getData: body ${response.body()?.map { it.toLeague() }}")
                emit(ResponseState.Success(response.body()?.map { it.toLeague() }))
            }
        } catch (e: Exception) {
            emit(ResponseState.Error(exception = TempException(e.message?:"")))
        }
    }

}