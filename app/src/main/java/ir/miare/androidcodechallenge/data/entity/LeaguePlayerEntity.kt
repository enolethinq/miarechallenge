package ir.miare.androidcodechallenge.data.entity

sealed class ListItem

data class PlayerEntity(
    var name: String,
    val team: TeamEntity,
    val totalGoal: Int
) : ListItem()

data class LeagueEntity(
    val name: String,
    val country: String,
    val rank: Int,
    val total_matches: Int,
    var average: Int = Int.MIN_VALUE
) : ListItem()

data class TeamEntity(
    val name: String,
    val rank: Int
)

data class LeagueData(
    val league: League,
    val players: List<Player>
)

data class League(
    val name: String="",
    val country: String="",
    val rank: Int=Int.MIN_VALUE,
    val total_matches: Int=Int.MIN_VALUE,
    var players: List<Player>
)

data class Player(
    val name: String,
    val total_goal: Int,
    val team: Team
)

data class Team(
    val name: String,
    val rank: Int
)
