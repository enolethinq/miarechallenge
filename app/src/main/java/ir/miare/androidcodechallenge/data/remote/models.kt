package ir.miare.androidcodechallenge.data.remote

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


data class PlayerLeagueResponse(
    @JsonProperty("league") var league: LeagueResponse,
    @JsonProperty("players") var players: List<PlayerResponse>
)

data class LeagueResponse(
    @JsonProperty("name") val name: String,
    @JsonProperty("country") val country: String,
    @JsonProperty("rank") val rank: Int,
    @SerializedName("total_matches") val total_matches: Int,
)
@Parcelize
data class PlayerResponse(
    @JsonProperty("name") val name: String,
    @JsonProperty("team") val team: TeamResponse,
    @JsonProperty("total_goal") val total_goal: Int
) : Parcelable

@Parcelize
data class TeamResponse(
    @JsonProperty("name") val name: String,
    @JsonProperty("rank") val rank: Int
) : Parcelable
