package ir.miare.androidcodechallenge.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.data.entity.LeagueEntity
import ir.miare.androidcodechallenge.data.entity.ListItem
import ir.miare.androidcodechallenge.data.entity.Player
import ir.miare.androidcodechallenge.data.entity.PlayerEntity
import ir.miare.androidcodechallenge.databinding.ItemLeagueBinding
import ir.miare.androidcodechallenge.databinding.ItemPlayerBinding

class LeaguePlayerAdapter(private val hideHeader: Boolean = false): ListAdapter<League, RecyclerView.ViewHolder>(LeagueDiffCallback()) {
    private val TAG = LeaguePlayerAdapter::class.java.simpleName
    private var lastHeaderPosition = 1

    companion object {
        private const val VIEW_TYPE_LEAGUE = 0
        private const val VIEW_TYPE_PLAYER = 1
        private const val VIEW_TYPE_ONLY_PLAYER = 2
    }

    inner class PlayerViewHolder(private val binding: ItemPlayerBinding): RecyclerView.ViewHolder(binding.root) {
        fun bindPlayer(entity: Player) {
            Log.d(TAG, "bindPlayer: ${entity.name} , ${entity.total_goal}")
            binding.apply {
                playerName.text = entity.name
                teamName.text = entity.team.name
                rank.text = entity.team.rank.toString()
                goalsNumber.text = entity.total_goal.toString()
            }
        }
    }

    inner class LeagueViewHolder(private val binding: ItemLeagueBinding): RecyclerView.ViewHolder(binding.root) {
        fun bindLeague(entity: League) {
            binding.apply {
                leagueName.text = entity.name
            }
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            VIEW_TYPE_PLAYER -> {
                PlayerViewHolder(
                    ItemPlayerBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                )
            }
            VIEW_TYPE_LEAGUE -> {
                LeagueViewHolder(
                    ItemLeagueBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                )
            }
            else -> {
                PlayerViewHolder(
                    ItemPlayerBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                )
            }

        }
    }

    override fun getItemCount(): Int {
        var count = 0
        currentList.forEach { league ->
            count += 1
            count += league.players.size
        }
        return count
    }

    override fun getItemViewType(position: Int): Int {
        if (!hideHeader) {
            var count = 0
            currentList.forEachIndexed { index, league ->
                count++
                if (position < count) return VIEW_TYPE_LEAGUE
                count += league.players.size
                if (position < count) return VIEW_TYPE_PLAYER
            }
            throw IllegalArgumentException("Invalid position")
        } else {
            var count = 0
            currentList.forEachIndexed { index, league ->
                count++
                return VIEW_TYPE_ONLY_PLAYER
            }
        }
        return VIEW_TYPE_ONLY_PLAYER
    }

    private lateinit var item: League

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (hideHeader) {
            var count = 0
            currentList.forEach { league ->
                val playerItemCount = league.players.size
                if (position < count + playerItemCount) {
                    val player = league.players[position - count]
                    (holder as PlayerViewHolder).bindPlayer(player)
                    return
                }
                count += playerItemCount
            }
        } else {
            when (holder) {
                is LeagueViewHolder -> {
                    item = currentList[holder.adapterPosition%3]
                    holder.bindLeague(item)
                    lastHeaderPosition = holder.adapterPosition + 1
                }
                is PlayerViewHolder -> {
                    holder.bindPlayer(item.players[(holder.adapterPosition - lastHeaderPosition)%3])
                }
            }

        }
    }

    class LeagueDiffCallback : DiffUtil.ItemCallback<League>() {
        override fun areItemsTheSame(oldItem: League, newItem: League): Boolean {
            return oldItem.players == newItem.players
        }

        override fun areContentsTheSame(oldItem: League, newItem: League): Boolean {
            return oldItem == newItem
        }
    }
}
