package ir.miare.androidcodechallenge.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ir.miare.androidcodechallenge.R
import ir.miare.androidcodechallenge.data.ResponseState
import ir.miare.androidcodechallenge.data.constant.SortMode
import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.data.entity.Player
import ir.miare.androidcodechallenge.databinding.FragmentRankingBinding
import ir.miare.androidcodechallenge.ui.adapter.LeaguePlayerAdapter

@AndroidEntryPoint
class RankingFragment() : Fragment() {
    private val TAG = RankingFragment::class.java.simpleName

    private val viewModel: SharedRankingViewModel by activityViewModels()

    private var _binding: FragmentRankingBinding? = null
    private val binding get() = _binding
    private lateinit var dataList: List<League>
    private lateinit var leagueAdapter: LeaguePlayerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i(TAG, "onCreateView: ")
        val view = inflater.inflate(R.layout.fragment_ranking, container, false)
        _binding = FragmentRankingBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(TAG, "onViewCreated: ")
        initObservers()
    }

    private fun initObservers() {
        Log.i(TAG, "initObservers: called")
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            whenCreated {
                viewModel.playerLeagueData.collect {
                    when(it) {
                        is ResponseState.Loading -> {
                            Log.d(TAG, "initObservers: playerLeagueData loading")
                            binding?.pbLoading?.show()
                        }
                        is ResponseState.Success -> {
                            Log.d(TAG, "initObservers: playerLeagueData success ${it.data?.size}")
                            binding?.pbLoading?.hide()
                            dataList = it.data?: emptyList()
                            setupRv(it.data)
                        }
                        is ResponseState.Error -> {
                            Log.e(TAG, "initObservers: playerLeagueData error ${it.exception?.errorMessage}")
                            binding?.pbLoading?.hide()
                            Toast.makeText(requireContext(), it.exception?.errorMessage, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewModel.sortingState.collect {
                when (it) {
                    SortMode.TEAM_LEAGUE_RANKING -> {
                        Log.d(TAG, "initObservers: sort: TEAM_LEAGUE_RANKING")
                        setupRv(viewModel.sortLeaguesAndPlayers(dataList))
                    }
                    SortMode.MOST_SCORE -> {
                        Log.d(TAG, "initObservers: sort: MOST_SCORE")

                        setupRv(viewModel.sortByTotalGoals(dataList), true)
                    }
                    SortMode.AVERAGE_PER_MATCH -> {
                        Log.d(TAG, "initObservers: sort: AVERAGE_PER_MATCH")
                        setupRv(viewModel.sortLeaguesByAveGoals(dataList))
                    }
                    SortMode.NONE -> {
                        Log.d(TAG, "initObservers: sort: NONE")
                        if (this@RankingFragment::dataList.isInitialized)
                                setupRv(dataList)
                    }
                }
            }
        }
    }


    private fun setupRv(list: List<League>?, hideHeader: Boolean = false) {
        list?.map { it.players.map { Log.d(TAG, "initObservers: nacm ${it.name} : ${it.total_goal}") } }
        Log.i(TAG, "setupRv: called ${list?.size}")
        leagueAdapter = LeaguePlayerAdapter(hideHeader)
        binding?.rvLeaguePlayer?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = leagueAdapter
            leagueAdapter.submitList(null)
            leagueAdapter.submitList(list)
            setHasFixedSize(true)
        }
    }

    override fun onDestroyView() {
        Log.i(TAG, "onDestroyView: called")
        super.onDestroyView()
        _binding.let { _binding = null }
    }
}