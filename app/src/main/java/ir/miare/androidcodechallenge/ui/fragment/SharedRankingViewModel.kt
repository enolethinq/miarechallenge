package ir.miare.androidcodechallenge.ui.fragment

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.miare.androidcodechallenge.data.ResponseState
import ir.miare.androidcodechallenge.data.constant.SortMode
import ir.miare.androidcodechallenge.data.entity.League
import ir.miare.androidcodechallenge.domain.playerLeague.PlayerLeagueUseCase
import ir.miare.androidcodechallenge.domain.playerLeague.SortUseCase
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SharedRankingViewModel @Inject constructor(
    private val playerLeagueUseCase: PlayerLeagueUseCase,
    private val sortUseCase: SortUseCase
) : ViewModel() {
    private val TAG = SharedRankingViewModel::class.java.simpleName

    private val _sortingState = MutableStateFlow(SortMode.NONE)
    val sortingState = _sortingState.asStateFlow()

    fun updateSortingState(sortingMode: SortMode) {
        Log.d(TAG, "updateSortingState: $sortingMode")
        _sortingState.value = sortingMode
    }

    private val _playerLeagueData = MutableSharedFlow<ResponseState<List<League>>>(replay = 1)
    val playerLeagueData = _playerLeagueData.asSharedFlow()

    private fun getData() {
        Log.i(TAG, "getData: called")
        viewModelScope.launch {
            playerLeagueUseCase.getData().collect {
                _playerLeagueData.emit(it)
            }
        }
    }

    fun sortLeaguesByAveGoals(list: List<League>) = sortUseCase.sortLeaguesByAveGoals(list)
    fun sortLeaguesAndPlayers(list: List<League>) = sortUseCase.sortLeaguesAndPlayers(list)
    fun sortByTotalGoals(list: List<League>) = sortUseCase.sortByTotalGoals(list)

    init {
        getData()
    }
}